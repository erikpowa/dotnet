#!/bin/sh
sudo sh -c 'echo "deb [arch=amd64] https://apt-mo.trafficmanager.net/repos/dotnet/ trusty main" > /etc/apt/sources.list.d/dotnetdev.list'

LINE='deb http://security.ubuntu.com/ubuntu trusty-security main'
FILE=/etc/apt/sources.list.d/trusty.list
grep -q "$LINE" "$FILE" || echo "$LINE" >> "$FILE"

sudo apt-key adv --keyserver apt-mo.trafficmanager.net --recv-keys 417A0893
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install dotnet-dev-1.0.0-preview2-003107